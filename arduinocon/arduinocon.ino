#include <Arduino.h>

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600); // 9600 baud is fine for our purposes
}

// the loop function runs over and over again forever
void loop() {
  static char playing = 0; // playing indicates whether the computer is currently playing a song
  static char songs = 0; // number of songs the computer has

  if (!songs)
  {
    digitalWrite(LED_BUILTIN, LOW);
    Serial.write(0x42); // 0x42 indicates to the computer that the arduino is attempting to establish a connection
    delay(500);
    if (Serial.read() == 0x42) { // if the computer responds with 42, the next byte will be the number of songs
      songs = Serial.read();
      Serial.write(songs); // respond to the computer with the number of songs to confirm
    }
    else return;
  }
  else
  {
    digitalWrite(LED_BUILTIN, HIGH);
  }

  if (Serial.peek() == 0x43) // receiving a 0x43 indicates the computer is terminating and resets the arduino
  {
    songs = 0;
    playing = 0;
    return;
  }

  if (playing) { // if the computer is playing...
    if (Serial.read() == 0xC8) playing = 0; // we should wait for the computer to send us a sound finished signal (0xC8)

    return; // otherwise do nothing
  }

  // depending on the buttons pressed play different songs
  for (int i = 0; i < songs; ++i)
  {
    if (digitalRead(i + 2))
    {
      Serial.write(i);
      playing = 1;
    }
  }
}
