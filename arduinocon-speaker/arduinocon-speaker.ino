#include <Arduino.h>

#include "SD.h"
#define SD_ChipSelectPin 4
#include "TMRpcm.h"
#include "SPI.h"

int pins_used[] = {2, 3, 5, 6, 7, 8, 10};
TMRpcm tmrpcm;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  tmrpcm.speakerPin = 9;
  SD.begin(SD_ChipSelectPin);
  Serial.begin(9600); // 9600 baud is fine for our purposes
  tmrpcm.setVolume(5);
}

// the loop function runs over and over again forever
void loop() {
  // depending on the buttons pressed play different songs
  for (int i = 0; i < sizeof(pins_used) / sizeof(int); ++i)
  {
    int pin = pins_used[i];
    if (digitalRead(pin))
    {
      char path[10] = {0};
      sprintf(path, "%d.wav", i);
      tmrpcm.play(path);
    }
  }
}
