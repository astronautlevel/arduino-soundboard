#!/usr/bin/env python3
import serial
import serial.tools.list_ports
import os
import time
import struct
import signal
import sys

from playsound import playsound

sound_path = 'sounds/' 

def main():
    global comm

    path = ""

    while path == "":
        ports = serial.tools.list_ports.comports()
        for port in ports:
            if "Arduino" in port.manufacturer:
                path = port.device

    print(f"Waiting for Arduino connection on {path}...")

    comm = serial.Serial(path)
    sounds = os.listdir(sound_path) # get list of all sounds
    sounds = sorted(sounds) # sort songs alphabetically

    num = len(sounds)

    while struct.unpack('>B', comm.read())[0] != 0x42: # wait for sync byte from arduino
        print(struct.unpack('>B', comm.read())[0])

    comm.read(comm.in_waiting) # discard any bytes that may be waiting to be read

    comm.write(struct.pack('>BB', 0x42, num)) # send 0x42 and num to indicate syncing + number of songs 

    anum = struct.unpack('>B', comm.read())[0] # if arduino responds with a different number of songs something is wrong

    if anum != num:
        print("Serial communications are either faulty or the arduino is programmed improperly")
    else:
        print(f"Connection successfully established on {path}")

    while True:
        song = struct.unpack('>B', comm.read())[0] # read sound arduino wants us to play
        print(f"Sound received. Index: {song}. Name: {sounds[song]}")
        playsound(sound_path + sounds[song])
        print("Sending sound finished signal...") 
        comm.write(b'\xC8') # 0xC8 tells the arduino the computer has finished playing the sound
        comm.read(comm.in_waiting)

def close_handle(sig, frame):
    global comm
    comm.write(b'\x43') # Indicate to arduino that program is terminating
    comm.close() # Close connection and exit
    sys.exit(0)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, close_handle) # run close_handle on ctrl + C (SIGINT)
    main()
